# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 09:31:21 2017
@title: JLR HPI
@author: Charles Demontigny
@version: Python 3.6
"""

# Import packages --------------------------------------------------------------
import numpy as np
import pandas as pd
import os
import statsmodels.api as sm
import sys
import click
import time

import models.hpi_models as hp
from data.make_dataset import load_dataset
from data.date_range import date_range_quarter, date_range_semester, create_date_range
from visualization.visualize import tsplot
from models.hpi_models import rolling_mean

# Click command ----------------------------------------------------------------
@click.command()
@click.option('-p', '--path', default = 'test')
@click.option('-t', '--type_', default = 'rmr')
@click.option('-s', '--secteur_', default = 'montreal')
@click.option('-c', '--categorie_', default = "'2A'")
@click.option('-z', '--period', default = "month")
@click.option('-r', '--rollingmean', default = False)
@click.option('-n', '--rollingmean_n', default = 6)
@click.option('-d', '--mindate', default = '20080101')
@click.option('-f', '--maxdate', default = '20180630')
def run_jlrhpi(path, type_, secteur_, categorie_, period, rollingmean, rollingmean_n, mindate, maxdate):

    # timer
    time0 = time.time()

    # Load data --------------------------------------------------------------------
    df = load_dataset(typ = type_, secteur=secteur_, categorie=categorie_, min_date = mindate, max_date = maxdate)
    #print("Ceci est la taille du df: %s" % str(df.shape))

    # Clean Oracle dataset ---------------------------------------------------------
    click.secho(f"\nNettoyage des donnees...", bold=True)
    clean_df = hp.cleanSQLoutput(df)
    #print("Ceci est la taille du clean_df: %s" % str(clean_df.shape))

    # Compute WRS ------------------------------------------------------------------
    click.secho(f"\nLe calcul du JLR HPI est en cours...", bold=True)
    hpi = hp.wrs_model(clean_df, id_ = "id_", time = period)
    #print("Finalement, ceci est la taille du hpi: %s" % str(hpi.shape))
    date = create_date_range(period, mindate, maxdate)
    output = pd.DataFrame({'date':date, 'indice': hpi})

    # Compute moving average
    if rollingmean:
        output = rolling_mean(output, 'indice', 'date', N = rollingmean_n)

    # Save as csv
    click.secho(f"\nSauvegarde...", bold=True)
    output.to_csv("../data/processed/" + path + '.csv', index = False)

    tsplot(df = output, path = path)
    time1 = time.time()
    delta = np.round(time1 - time0, decimals = 1)

    print('Terminé! Le code a pris : %s secondes' % delta)

# Run --------------------------------------------------------------------------
if __name__ == '__main__':
    run_jlrhpi()
