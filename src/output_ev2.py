"""
Created on Mon August 6th
@title: JLR HPI
@author: Charles Demontigny
@version: Python 3.6
"""

# Import modules ---------------------------------------------------------------
import pandas as pd
import numpy as np
import os
import statsmodels.api as sm
import sys
import click
import time
import itertools
from datetime import datetime

import models.hpi_models as hm
from models.chain_process import multi_chain, base_index_generator
from data.make_dataset import load_dataset, date_range_quarter
from data.date_range import last_day_last_month

# Main function ----------------------------------------------------------------
def output_ev2(ra_list = list(np.arange(1, 18)), catbat = "'2A', '2B', '2C', '3C'",
               period = 'year', mindate = '20040101', basedate = '20071231',
               current_date = '20081231', years = np.arange(2008, 2019),
               path = "data/processed/jindex_for_evia.csv"):
    output = pd.DataFrame()
    for ra in ra_list:

        print("Region administrative: {}".format(ra))
        # Base Index
        base_index, _ = base_index_generator('ra', ra, catbat, period, mindate, basedate)

        # Loop over years
        for year in years:
            current_date = "".join([str(year), "1231"])
            df = load_dataset(typ = 'ra', secteur = ra, categorie = catbat,
                            min_date = mindate, max_date = current_date, print = False)

            # Clean DataFrame
            clean_df = hm.cleanSQLoutput(df)

            # Set variables
            price = clean_df['price']
            timevar = clean_df[period]
            id_ = clean_df['id_']

            beta_inv, rps = hm.update_base_index(id_, price, timevar, base_index)

            # Generate output DataFrame
            base_index = base_index.append(
                pd.DataFrame({'date':[rps.time1.max()], 'indice':[beta_inv]})
            )

            base_index['catbat'] = 'TOUT'
            base_index['type_geo'] = 'ra'
            base_index['desc_geo'] = ra
            base_index['type_periode'] = period

            base_index.reset_index(drop = True, inplace = True)
        output = output.append(base_index, ignore_index = True)

    output.to_csv(path)
