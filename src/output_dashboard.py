"""
Created on Thu Oct 26 09:31:21 2017
@title: JLR HPI
@author: Charles Demontigny
@version: Python 3.6
"""

# Import packages ---------------------------------------------------------
import pandas as pd
import numpy as np
import os
import statsmodels.api as sm
import sys
import click
import time
import itertools
from datetime import datetime

import models.hpi_models as hm
from models.chain_process import multi_chain
from data.make_dataset import load_dataset, date_range_quarter
from data.date_range import last_day_last_month

# time code
t0 = time.time()

# Setup input ------------------------------------------------------------------

# Secteur
rmr = ['montreal', 'quebec', 'trois-rivieres', 'gatineau', 'sherbrooke', 'saguenay']

# Categorie
catbat = ["'2A'", "'3C'", "'2A', '3C'"]

# Period
period = ['month', 'year']

# Dates
mindate = '20080101'
maxdate = '20180331'
current_date = last_day_last_month()

# Create output DataFrame ------------------------------------------------------

df = multi_chain(df = None, type = 'rmr', secteur = rmr, categorie = catbat,
                 period = period, mindate = mindate, maxdate = maxdate,
                 current_date = current_date, verbose = True)

# Save output ------------------------------------------------------------------

print(df)

#path = "hpi_" + datetime.now().date().strftime("%Y-%m-%d") + '.csv'
path = 'test_jindex.csv'
df.to_csv("../data/processed/" + path, index = False)
#
# Process time -------------------------------------------------------------
t1 = time.time()
delta = np.round((t1 - t0)/60, decimals = 2)
print("Terminé! Le HPI a pris {} minutes a rouler.".format(delta))
