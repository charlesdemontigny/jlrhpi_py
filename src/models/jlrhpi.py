# Import packages --------------------------------------------------------------
import pandas as pd
import numpy as np
import pandas as pd
import os
import statsmodels.api as sm
import sys
import click
import time
import itertools

import models.hpi_models as hm
from data.make_dataset import load_dataset
from data.date_range import date_range_quarter, date_range_semester, create_date_range
from models.hpi_models import rolling_mean
from visualization.visualize import tsplot

# Define helper function -------------------------------------------------------
def jlrhpi(model, type_, secteur_, categorie_, rollingmean, rollingmean_n = 6,
                period = 'month', mindate = '20080101', maxdate = '20171231'):

    # Testing
    #condition_type = (period)
    #assert condition_type, "Le type n'est pas conforme."
    condition_model = model == "wrs" or model == "iv"
    assert condition_model, "Le model doit etre wrs ou iv."

    # Load data
    df = load_dataset(typ = type_, secteur=secteur_, categorie=categorie_,
                    min_date = mindate, max_date = maxdate, print = False)

    # Clean and compute WRS if length != 0
    if df.shape[0] != 0:
        clean_df = hm.cleanSQLoutput(df)
        if model == "wrs":
            hpi = hm.wrs_model(clean_df, id_ = "id_", time = period)
        if model == "iv":
            hpi = hm.iv_model(clean_df, id_ = "id_", time = period)
    else:
        hpi = np.nan

    # Create date_range var
    date = create_date_range(period, mindate, maxdate)
    # if period == 'month':
    #     date = pd.date_range(mindate, maxdate, freq = 'MS')
    #     date = pd.Series(date)
    #     date = date.dt.date
    #
    # if period == 'quarter':
    #     min_quarter = str(pd.to_datetime(mindate).year) + "-" + str(int(pd.Series(pd.to_datetime(mindate)).dt.quarter))
    #     max_quarter = str(pd.to_datetime(maxdate).year) + "-" + str(int(pd.Series(pd.to_datetime(maxdate)).dt.quarter))
    #     date = date_range_quarter(min_quarter, max_quarter)
    #
    # if period == "semester":
    #     min_semester = str(pd.to_datetime(mindate).year) + "-"
    #     if int(pd.Series(pd.to_datetime(mindate)).dt.quarter) <= 2:
    #         min_semester += "1"
    #     else: min_semester += "2"
    #
    #     max_semester = str(pd.to_datetime(maxdate).year) + "-"
    #     if int(pd.Series(pd.to_datetime(maxdate)).dt.quarter) <= 2:
    #         max_semester += "1"
    #     else: max_semester += "2"
    #     date = date_range_semester(min_semester, max_semester)
    #
    # if period == 'year':
    #     min_year = pd.to_datetime(mindate).year
    #     max_year = pd.to_datetime(maxdate).year + 1
    #     date = np.arange(min_year, max_year).astype(str)

    # Generate output DataFrame
    try: output = pd.DataFrame({'date':date,
                           'indice': hpi,
                           'catbat': categorie_,
                           'type_geo': type_,
                           'desc_geo': secteur_,
                           'type_periode': period})
    except ValueError:
        output = pd.DataFrame({'date':date,
                               'indice': np.nan,
                               'catbat': categorie_,
                               'type_geo': type_,
                               'desc_geo': secteur_,
                               'type_periode': period})

    # Compute moving average
    if rollingmean:
        output = rolling_mean(output, 'indice', 'date', N = rollingmean_n)

    # Save as csv
    return output

def multi_jlrhpi(df, model, type_, secteur, categorie, period,rolling_mean = False,
                mindate = '20080101', maxdate = '20180331', verbose = False):
    """
    Cette fonction se veut un wrapper de jlrhpi. La fonction cree un loop over
    les inputs habituelles de jlrhpi que l'on input en arrays au lieu d'input
    unique.
    """
    assert isinstance(type_, str), "Type doit etre un string"
    #assert isinstance(secteur, list), "Secteur doit etre une list"
    assert isinstance(categorie, list), "Categorie doit etre une list"
    assert isinstance(period, list), "Period doit etre une list"

    if df is None:
        df = pd.DataFrame()
    # Creer toutes les combinaisons de tous les secteurs/categories
    comb = list(itertools.product(secteur, categorie, period))

    # Loop over comb
    for region, catbat, temp in comb:
        if verbose:
            print(region, catbat, temp)
        df = df.append(jlrhpi(model = model,
                                  type_ = type_,
                                  secteur_=region,
                                  categorie_= catbat,
                                  rollingmean = False,
                                  period = temp,
                                  mindate = mindate,
                                  maxdate = maxdate))
        # except ValueError:
        #     print("Pas assez de transactions dans la combinaison zone/time/composition")

    return df
