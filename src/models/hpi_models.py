# Functions -------------------------------------------------------------------
import numpy as np
import pandas as pd
import os
import statsmodels.api as sm

# cleanSQLoutput
def cleanSQLoutput(df, id_name = "TRA_ID_UEF",
                   datevar = "TRA_DT_PUBLIE",
                   numero = "TRA_NUMERO",
                   num = False):

    idd = df[id_name][df.duplicated(id_name)]
    df = df[df[id_name].isin(idd)]

    id_ = df[id_name]
    year = df[datevar].astype(str).str.slice(start=0, stop=4)
    month = pd.Series(
            year + "-" +
            df[datevar].astype(str).str.slice(start=5, stop=7).astype(str) +
            "-01")

    # Quarter
    q = df[datevar].dt.quarter.astype(str)
    quarter = pd.Series(year + "-" + q)

    # Semester
    semester = []
    for y, q in zip(year, q):
        if int(q) <= 2:
            semester.append(y + "-1")
        if int(q) > 2:
            semester.append(y + "-2")

    # Create output
    if num:
        numero = df[numero]
        result =  pd.DataFrame({'id_': id_,
                            'numero':numero,
                             'date' : df.TRA_DT_PUBLIE,
                             'price' : df.TRA_MNT_TRANSACTION,
                             'year' : year,
                             'month' : month,
                             'quarter': quarter,
                             'semester':semester})
    else:
        result =  pd.DataFrame({'id_': id_,
                         'date' : df.TRA_DT_PUBLIE,
                         'price' : df.TRA_MNT_TRANSACTION,
                         'year' : year,
                         'month' : month,
                         'quarter': quarter,
                         'semester':semester})

    result.reset_index(inplace = True)

    return(result)

# repsaledata
def repsaledata(id_, price, timevar, numero = None, priceLog = True, num = False):

    """ Cette fonction transforme un dataset avec une vente par ligne a un dataset de ventes pairees"""

    df = pd.DataFrame({"id_":id_, "time": timevar, "price":price, "numero":numero})

    df = df[['id_', 'time', 'price', 'numero']]

    df = df.sort_values(by = ['id_', 'time'])
    df.reset_index(inplace = True)
    if priceLog:
        df.price = np.log(df.price)

    n = df.shape[0]
    data0 = df.loc[np.arange(0, n-1), ]
    data0.reset_index(inplace = True)
    data1 = df.loc[np.arange(1, n), ]
    data1.time.sort_index(inplace = True)
    data1.reset_index(inplace = True)

    rsale = (data0.id_ == data1.id_) & (data0.time < data1.time)
    data0 = data0[rsale]; data1 = data1[rsale]

    if num:
        return pd.DataFrame({'id_':data1.id_,
                             'numero':data1.numero,
                             'time0' : data0.time,
                             'time1' : data1.time,
                             'price0' : data0.price,
                             'price1' : data1.price})

    return pd.DataFrame({'id_':data1.id_,
                         'time0' : data0.time,
                         'time1' : data1.time,
                         'price0' : data0.price,
                         'price1' : data1.price})

# design_matrix
def design_matrix(df):
   time0 = np.array(df.time0, dtype = pd.Series)
   time1 = np.array(df.time1, dtype = pd.Series)
   timevar = np.unique(np.concatenate((time0, time1), axis = 0))

   nt = len(timevar)
   n = df.shape[0]

   xmat = np.zeros((n, nt - 1))
   for j in range(1, n):
       xmat[j, time1[j] == timevar[1:(np.shape(timevar)[0]+1)]] = 1
       xmat[j, time0[j] == timevar[1:np.shape(timevar)[0]]] = -1

   return(xmat)

# design_matrix_iv
def design_matrix_iv(df):

    time0 = np.array(df.time0, dtype = pd.Series)
    time1 = np.array(df.time1, dtype = pd.Series)
    price0 = np.array(df.price0, dtype = pd.Series)
    price1 = np.array(df.price1, dtype = pd.Series)

    timevar = np.unique(np.concatenate((time0, time1), axis = 0))

    nt = len(timevar)
    n = len(price1)

    xmat = np.zeros((n, nt - 1))

    for j in range(1, nt):
        xmat[time1 == timevar[j], j - 1] = price1[time1 == timevar[j]]
        xmat[time0 == timevar[j], j - 1] = -price0[time0 == timevar[j]]

    return xmat

# linear_model
def linear_model(xmat, dy):
    return(np.matmul(np.matmul(np.linalg.inv(
            np.matmul(np.transpose(xmat), xmat)), np.transpose(xmat)), dy))


# wrs_model
def wrs_model(df, id_ = "id", price = "price", time = "month", priceLog = True, round = 4):

    """ Set variables """
    # Variables de base
    price = df[price]
    timevar = df[time]
    id_ = df[id_]

    # DataFrame de vente pairees
    rsp = repsaledata(id_, price, timevar)

    # On drop les inf qui peuvent etre creer si un prix est zero.
    rsp = rsp.replace([np.inf, -np.inf], np.nan).dropna()

    # S'il n'y a pas de ventes pairees, la fonction retourne NaN
    if rsp.shape[0] == 0:
        return np.nan

    try:
        # Set matrix
        xmat = design_matrix(rsp)
        dy = rsp.price1 - rsp.price0
        """ Three stage regression """
        # First stage
        beta = linear_model(xmat, dy)

    except (np.linalg.linalg.LinAlgError, MemoryError, ValueError) as exception:
        return np.nan

    else:
        # Second stage
        u = dy - np.matmul(xmat, beta)
        x = (pd.to_datetime(rsp.time1) - pd.to_datetime(rsp.time0)).dt.days
        x = sm.add_constant(x)
        model = sm.OLS(np.absolute(u), x).fit()
        wgt = model.predict()
        weight = np.zeros([len(wgt), ])
        weight[wgt > 0] = wgt[wgt > 0]

        try:# Third stage
            fit = sm.WLS(dy, xmat, weights = weight).fit()
            coef = np.append([0], fit.params)

            return np.round(np.exp(coef), decimals = round)

        except MemoryError:
            return np.nan

# IV_model
def iv_model(df, id_ = "id_", price = "price", time = "month", round = 4):

    """Set variables"""
    # Variables bases
    price = df[price]
    timevar = df[time]
    id_ = df[id_]

    # DataFrame de vente pairees
    rps = repsaledata(id_, price, timevar, priceLog = False)
    rps.replace([np.inf, -np.inf], np.nan).dropna()

    # S'il n'y a pas de ventes pairees, la fonction retourne NaN
    if rps.shape[0] == 0:
        return np.nan

    try:
        # Set matrix for estimation
        zmat = design_matrix(rps)
        xmat = design_matrix_iv(rps)
        y = np.zeros([rps.shape[0], ])
        y[rps.time0 == np.sort(np.unique(rps.time0))[0]] = rps.price0[rps.time0 == np.sort(np.unique(rps.time0))[0]]
        """Three Stage Least-Square"""
        # First Stage (Z'X)^-1 Z'Y
        z_prime = np.transpose(zmat)
        z_prime_x = np.matmul(z_prime, xmat)
        z_prime_x_inv = np.linalg.inv(z_prime_x)

        beta = np.matmul(np.matmul(z_prime_x_inv, z_prime), y)

    except(np.linalg.linalg.LinAlgError, MemoryError, ValueError) as exception:
        return np.nan

    else:
        # Second Stage
        u = y - np.matmul(xmat, beta)
        delta_time = (pd.to_datetime(rps.time1) - pd.to_datetime(rps.time0)).dt.days
        delta_time = sm.add_constant(delta_time)
        model = sm.OLS(np.absolute(u), delta_time).fit()
        wgt = model.predict()
        wgt = wgt ** -1

        z_prime_omega = [zmat[i, :] * wgt[i] for i in np.arange(len(wgt))]
        z_prime_omega = np.transpose(z_prime_omega)

        try:
            # Third Stage
            beta_final = np.matmul(
                    np.matmul(
                        np.linalg.inv(
                            np.matmul(z_prime_omega, xmat)), z_prime_omega), y)

            coef = np.append([1], beta_final ** -1)
            return np.round(coef, decimals = round)
        except MemoryError:
            return np.nan

# Update base index
def update_base_index(id_, price, timevar, base_index):
    """
    Cette fonction update le base index a la maniere IV.
    """

    # Repeat sales
    rps = repsaledata(id_, price, timevar, priceLog = False)
    rps.replace([np.inf, -np.inf], np.nan).dropna()

    # Create matrix
    # Y: vector colonne des ventes time0 avec price0/beta
    y_temp = pd.merge(rps, base_index,
                        how = 'left', left_on = "time0", right_on = "date")
    y = np.transpose(np.matrix(y_temp.price0 / y_temp.indice))

    # X: vector colonne de taille n x 1 avec des zeros lorsqu'il n'y a pas de ventes
    # a la periode t et le price autrement.
    xmat = design_matrix_iv(rps)
    x = xmat[:, xmat.shape[1] -1 ] # Return last column of xmat
    x = np.transpose(np.matrix(x))

    # Z: vector colonne de taille n x 1 avec des zeros des 0 (zeros) lorsqu'il n'y a
    # pas de ventes a la periode t et 1 autrement.
    zmat = design_matrix(rps)
    z = zmat[:, zmat.shape[1] - 1] # Return last column of zmat
    z_prime = np.matrix(z)

    # Estimer (Z'X)^-1 Z'Y
    z_prime_x = z_prime * x
    beta = ((z_prime * x) ** -1) * z_prime * y
    beta_inv = np.round(1 / beta[0,0], 4)

    return beta_inv, rps

# rollingmean
def rolling_mean(df, indice, date, N):
    x = df[indice].values
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return pd.DataFrame({'date': df[date][N-1:], 'indice':(cumsum[N:] - cumsum[:-N]) / float(N)})
