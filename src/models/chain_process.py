"""
@date: 2018-05-28
@title: Chain procedure
@author: Charles Demontigny
@version: Python 3.6
"""

# Import packages --------------------------------------------------------------
import pandas as pd
import numpy as np
import os
import pathlib as pl
import itertools

import models.hpi_models as hm
from data.make_dataset import load_dataset
from data.date_range import create_date_range, date_to_period

# Chain procedure --------------------------------------------------------------

# base_index_generator
def base_index_generator(type, secteur, categorie, period, mindate, maxdate):
    """Generate base_index if necessary"""

    # Load or generate base_index
    #import pdb; pdb.set_trace()
    specificite = "_".join([type, str(secteur), categorie, period])

    base_index_path = pl.Path("../data/interim/chain/base_index_" + specificite + ".csv")

    # if base_index_path.is_file():
    #     base_index = pd.read_csv(base_index_path)
    #     base_index['date'] = base_index['date'].astype(str)
    # else:

    # Load data
    df = load_dataset(typ = type, secteur = secteur, categorie = categorie,
                      min_date = mindate, max_date = maxdate, print = False)

    # Clean df
    clean_df = hm.cleanSQLoutput(df)

    # IV model
    base_hpi = hm.iv_model(clean_df, id_ = "id_", time = period)

    # Date
    date = create_date_range(period = period, mindate = mindate, maxdate = maxdate)

    # Generate output DataFrame
    base_index = pd.DataFrame({'date':date,
                           'indice':base_hpi})

    return base_index, base_index_path

# Chain
def chain(type, secteur, categorie, period, mindate, maxdate, current_date):
    """
    Cette fonction produit un HPI enchaine a la maniere de Standard&Poor
    """

    # Setup path
    if not os.path.exists('../data/interim/chain'):
        os.makedirs('../data/interim/chain')

    # Load or generate base_index
    base_index, base_index_path = base_index_generator(type, secteur, categorie, period,
                                        mindate, maxdate)

    # Dont override up to date index
    current_date_to_period = date_to_period(current_date, period)
    max_date_to_period = date_to_period(base_index.date.max(), period)

    if current_date_to_period == max_date_to_period:
        base_index['catbat'] = categorie
        base_index['type_geo'] = type
        base_index['desc_geo'] = secteur
        base_index['type_periode'] = period
        return base_index

    # Load dataset
    df = load_dataset(typ = type, secteur = secteur, categorie = categorie,
                    min_date = mindate, max_date = current_date, print = False)

    # Clean DataFrame
    clean_df = hm.cleanSQLoutput(df)

    # Set variables
    price = clean_df['price']
    timevar = clean_df[period]
    id_ = clean_df['id_']

    beta_inv = hm.update_base_index(id_, price, timevar, base_index)

    # Generate output DataFrame
    import pdb; pdb.set_trace()
    output = base_index.append(
        pd.DataFrame({'date':[rps.time1.max()], 'indice':[beta_inv]})
    )

    output['catbat'] = categorie
    output['type_geo'] = type
    output['desc_geo'] = secteur
    output['type_periode'] = period

    output.reset_index(drop = True, inplace = True)
    output.to_csv(base_index_path, index = False)

    return output

# Multi Chain
def multi_chain(df, type, secteur, categorie, period, mindate, maxdate,
                current_date, verbose = False):
    """
    Cette fonction est un wrapper de chain. La fonction cree un loop over
    les inputs habituelles de jlrhpi que l'on input en arrays au lieu d'input
    unique.
    """

    # Creer un DataFrame si aucun n'est fournis
    if df is None:
        df = pd.DataFrame()

    # Creer toutes les combinaisons de tous les secteurs/categories/periods
    comb = list(itertools.product(secteur, categorie, period))

    # Loop over comb
    for region, catbat, temp in comb:
        if verbose:
            print(region, catbat, temp)
        df = df.append(chain(type = type,
                             secteur = region,
                             categorie = catbat,
                             period = temp,
                             mindate = mindate,
                             maxdate = maxdate,
                             current_date = current_date))

    return df
