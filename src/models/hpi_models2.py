# Functions -------------------------------------------------------------------
import numpy as np
import pandas as pd
import os
import statsmodels.api as sm
import click
import cx_Oracle
import traceback

class Data_wrangling:
    """
    Une classe qui charge et nettoie les donnees. C'est une classe de base sous
    les modeles pour le HPI.
    """
    def __init__(self):
        self._load_done = False
        self._clean_done = False
        self._repsaledata_done = False

    def return_df(self):
        """Une fonction qui retourne le DataFrame contenant les transactions.

        Returns:
            Un Pandas DataFrame contenant les transactions.
        """
        return self.df

    def _parse_load_data_args(self, type, secteur, categorie, min_mnt, min_date,
                              max_date, print):
        """Analyse les args de load_data.

            Args:
                Les memes arguments que load_data.
            Raises:
                ValueError: Si le format des args n'est pas le bon.
        """
        if type not in ('rmr', 'ville', 'dr', 'ra', 'arr'):
            raise ValueError("Le type '{}' n'est pas conforme. Doit etre compris dans'rmr', 'ville', 'dr', 'ra', 'arr'.".format(type))

        if not isinstance(secteur, str):
            raise ValueError("Le secteur '{}' doit etre une string.".format(secteur))

        if not (isinstance(categorie, str) or isinstance(categorie, list)):
            raise ValueError("La categorie '{}' n'est pas une liste ou une string.".format(categorie))

        if (isinstance(categorie, str) and
                categorie not in ("'2A'", "'2B'", "'2C'", "'3C'")):
            raise ValueError("La categorie '{}' n'est pas parmis '2A', '2B', '2C' ou '3C'.".format(categorie))

        if (isinstance(categorie, list) and
            not all([cat in ("'2A'", "'2B'", "'2C'", "'3C'") for cat in categorie])):
            raise ValueError("Toutes les categories de la liste ne sont pas parmis '2A', '2B', '2C' ou '3C'.")

        try:
            int(min_mnt)
        except ValueError:
            raise ValueError("L'argument min_mnt doit etre un numerique.")
        else:
            if min_mnt < 0:
                raise ValueError("L'argument min_mnt doit etre plus grand que zero.")

        if ((not isinstance(min_date, str)) or (not isinstance(max_date, str))):
            raise ValueError("Les arguments de date doivent etre des string.")

        if ((len(min_date) != 8 or not min_date.isnumeric()) or
                (len(max_date) != 8 or not max_date.isnumeric())):
            raise ValueError("Les arguments de date doivent etre du format: 'YYYYmmdd'.")

        if not isinstance(print, bool):
            raise ValueError("L'argument print doit etre un boolean.")

    def load_dataset(self, type, secteur, categorie, min_mnt = 50000,
                     min_date = '20080101', max_date = '20180331', print = True):
        """Load dataset from Oracle giving the parameter set.

        Args:
            type: Le type de zone geographique tel que 'rmr', 'ville', 'dr', etc.
            secteur: Le nom de la zone geographique relie au type tel que 'Montreal'
                     pour ville ou '1' pour 'ra'.
            categorie: La categorie de batiment tel que '2A', '3C', '2B' ou '2C'.
                       Doit etre une string pour une categorie unique ou une liste
                       de strings pour plusieurs categories.
            min_mnt: Le montant minimal a considere pour garder une vente. Par
                     defaut le montant minimal d'une transaction est 50 000 $.
            min_date: La date de debut de la periode chargee. Doit avoir un
                      format de date 'YYYYmmdd'.
            max_date: La date de fin de la periode chargee. Doit avoir un
                      format de date 'YYYYmmdd'.
            print: Boolean indiquant si on imprime des etapes sur la console.

        Returns:
            Un Pandas DataFrame contenant toutes les transactions repondant aux
            criteres choisis en arguments.
        """

        # Test les inputs
        self._parse_load_data_args(type, secteur, categorie, min_mnt, min_date,
                                  max_date, print)

        # Variables settings
        rmr = {'quebec':421, 'saguenay':408, 'sherbrooke':433,
                'trois-rivieres': 442, 'montreal':462, 'gatineau':505}
        if isinstance(categorie, list):
            categorie = ",".join(categorie)

        # Oracle connection and query
        if print:
            click.secho(f"\nInitialisation de la connexion et creation de la requete.", bold=True)
        connection = cx_Oracle.connect( "jlrprod", 'rlj','azuorap3', encoding = 'UTF-8')

        query = """select TRA_TRANSACTION.TRA_MNT_TRANSACTION, TRA_TRANSACTION.TRA_DT_PUBLIE, TRA_TRANSACTION.TRA_ID_UEF, TRA_NUMERO, TRA_TRANSACTION.TRA_CAT_BAT
                    from tra_transaction
                    join municipalites_bsq on TRA_TRANSACTION.IMM_BSQ = MUNICIPALITES_BSQ.MUN_NO_BSQ
                    join municipalites_statcan_bsq on TRA_TRANSACTION.IMM_BSQ = MUNICIPALITES_STATCAN_BSQ.MUN_NO_BSQ
                    where tra_type=4 and tra_cat_bat in (""" + categorie + """) and to_char(tra_dt_publie,
                            'YYYYmmdd')> '""" + min_date + """' and
                            to_char(tra_dt_publie, 'YYYYmmdd') <= '""" + max_date +"""' and
                            TRA_IND_VENTE_MULTIPLE=0
                            and TRA_IND_VENTE_REPRISE=0
                            and TRA_IND_VENTE_LIE=0
                            and TRA_MNT_TRANSACTION >"""  + str(min_mnt)

        # Cette partie permet de filtrer sur des zones geographiques differentes.
        if type == 'rmr':
            query += """ and municipalites_bsq.MUN_NO_RMR = """ + str(rmr[secteur])

        if type == 'ville':
            query += """ and municipalites_bsq.MUN_BSQ_FUSION = """ + str(secteur)

        if type == 'dr':
            query += """ and DRIDU = """ + str(secteur)

        if type == 'ra':
            query += """ and municipalites_bsq.MUN_NO_RA = """ + str(secteur)

        if type == 'arr':
            query += """and imm_arv_id =  """ + str(secteur)
            query += """and imm_bsq_nom in ('MONTREAL','LEVIS')"""

        if print:
            click.secho(f"\nDebut du telechargement des donnees a partir de Oracle.", bold=True)

        self.df =  pd.read_sql(query.encode('utf-8'), con = connection)

        if any([x != 0 for x in self.df.isna().sum()]):
            print("Il y a des valeurs manquantes dans le Pandas DataFrame!")

        if (sum(self.df.TRA_MNT_TRANSACTION > 0) != len(self.df)):
            print("Il y a des montants de transaction de zero ou negative.")

        self._load_done = True

    def clean_SQL_output(self, id = 'TRA_ID_UEF', datevar = 'TRA_DT_PUBLIE',
                        numero = 'TRA_NUMERO'):
        """Une fonction qui remodele le DataFrame qui est retourne de la methode
           load_dataset.

           Args:
                id: L'ID unique d'un batiment. Doit etre une string.
                datevar: La variable contenant la date de la transaction.
                numero: Le numero unique de la transaction.
        """
        assert self._load_done, "load_dataset doit etre rouler avant cleanSQLoutput"

        if any([x != 0 for x in self.df.isna().sum()]):
            print("Il y a des valeurs manquantes dans le Pandas DataFrame!")

        df = self.df
        duplicated_id = df[id][df.duplicated(id)]
        df = df[df[id].isin(duplicated_id)]

        # Return period in the datevar
        year = df[datevar].astype(str).str.slice(start=0, stop=4)
        month = pd.Series(
                year + "-" +
                df[datevar].astype(str).str.slice(start=5, stop=7).astype(str) +
                "-01")
        q = df[datevar].dt.quarter.astype(str)
        quarter = pd.Series(year + "-" + q)
        semester = []
        for y, q in zip(year, q):
            if int(q) <= 2:
                semester.append(y + "-1")
            if int(q) > 2:
                semester.append(y + "-2")

        # Create output DataFrame
        output_df = pd.DataFrame({'id': df[id],
                            'numero':df[numero],
                             'date' : df[datevar],
                             'price' : df['TRA_MNT_TRANSACTION'],
                             'year' : year,
                             'month' : month,
                             'quarter': quarter,
                             'semester':semester})
        output_df.reset_index(drop = True, inplace = True)

        self.df = output_df

        if any([x != 0 for x in self.df.isna().sum()]):
            print("Il y a des valeurs manquantes dans le Pandas DataFrame!")

        self._clean_done = True

    def rep_sale_data(self, timevar, priceLog = True):
        """Cette methode transforme le clean Pandas DataFrame avec une ligne par
           transaction par un Pandas DataFrame avec une ligne par vente pairee.

        Args:
             timevar: Le nom de la colonne de la variable de temps a utilise. Doit prendre
                      la valeur 'month', 'quarter', 'semester' ou 'year'.
             priceLog: Boolean indiquand si la variable de prix doit etre
                       transformee en log ou non.

        Creates:
                Un Pandas DataFrame de ventes pairees.
        """
        assert self._load_done, "load_dataset doit etre roule avant repsaledata."
        assert self._clean_done, "cleanSQLoutput doit etre roule avant repsaledata."

        if not isinstance(priceLog, bool):
            raise ValueError("L'argument priceLog doit etre un boolean.")

        if any([x != 0 for x in self.df.isna().sum()]):
            print("Il y a des valeurs manquantes dans le Pandas DataFrame!")

        df = self.df
        col = ['date', 'id', 'month', 'numero', 'price', 'quarter', 'semester', 'year']
        if not all([c in df.columns for c in col]):
            raise ValueError("Toutes les colonnes '{}' ne sont pas presentent dans le DataFrame.")

        if timevar not in df.columns:
            raise ValueError("timevar invalide: {}".format(timevar))
        # Subset DataFrame pour ne garder la timevar a utilise pour generer les ventes pairees.
        sub_df = df[['id', timevar, 'price', 'numero']].sort_values(by = ['id', timevar])
        sub_df.reset_index(inplace = True, drop = True)

        if priceLog:
            sub_df['price'] = np.log(sub_df['price'])

        n = sub_df.shape[0]
        data0 = sub_df.loc[np.arange(0, n-1), ]
        data0.reset_index(inplace = True, drop = True)
        data1 = sub_df.loc[np.arange(1, n), ]
        data1[timevar].sort_index(inplace = True)
        data1.reset_index(inplace = True)

        rsale = (data0.id == data1.id) & (data0[timevar] < data1[timevar])
        data0 = data0[rsale]
        data1 = data1[rsale]

        df = pd.DataFrame({'id':data1['id'],
                             'time0' : data0[timevar],
                             'time1' : data1[timevar],
                             'price0' : data0['price'],
                             'price1' : data1['price']})

        if any([x != 0 for x in self.df.isna().sum()]):
            print("Il y a des valeurs manquantes dans le Pandas DataFrame!")
        if any([(col is np.inf or col is -np.inf) for col in df]):
            print("Il y a des valeurs inf ou -inf dans le Pandas Dataframe. Ces observations sont retirees de l'analyse.")
            df = df.replace([np.inf, -np.inf], np.nan).dropna()

        self.df = df
        self._repsaledata_done = True

    def design_matrix(self):
        """Cette methode cree la matrice des X de la regression Weighted Repeat Sale
           presentee dans l'article de Case et Shiller de 1989.

           Creates:
                   Une matrice de design.
        """
        assert self._load_done, "load_dataset doit etre roule avant design_matrix."
        assert self._clean_done, "cleanSQLoutput doit etre roule avant design_matrix."
        assert self._repsaledata_done, "repsaledata doit etre roule avant design_matrix."

        if any([x != 0 for x in self.df.isna().sum()]):
            print("Il y a des valeurs manquantes dans le Pandas DataFrame!")

        col = ('id', 'price0', 'price1', 'time0', 'time1')
        if any([x not in self.df.columns for x in col]):
            ValueError("Il y a un probleme avec le nom des colonnes du Pandas DataFrame df.")

        df = self.df

        time0 = np.array(df.time0, dtype = pd.Series)
        time1 = np.array(df.time1, dtype = pd.Series)
        timevar = np.unique(np.concatenate((time0, time1), axis = 0))

        nt = len(timevar)
        n = df.shape[0]

        xmat = np.zeros((n, nt - 1))
        for j in range(1, n):
            xmat[j, time1[j] == timevar[1:(np.shape(timevar)[0]+1)]] = 1
            xmat[j, time0[j] == timevar[1:np.shape(timevar)[0]]] = -1

        if sum(np.isnan(xmat)).sum() != 0:
            print("Il y a des valeurs manquantes dans la design_matrix")

        return np.matrix(xmat)

    def design_matrix_iv(self):
        """Cette methode cree la matrice des X de la regression par variable
           instrumental presentee dans l'article de Case de 1991 et dans la
           methodologie.

           Creates:
                   Une matrice de design.
        """
        assert self._load_done, "load_dataset doit etre roule avant design_matrix."
        assert self._clean_done, "cleanSQLoutput doit etre roule avant design_matrix."
        assert self._repsaledata_done, "repsaledata doit etre roule avant design_matrix."

        if any([x != 0 for x in self.df.isna().sum()]):
            print("Il y a des valeurs manquantes dans le Pandas DataFrame!")

        col = ('id', 'price0', 'price1', 'time0', 'time1')
        if any([x not in self.df.columns for x in col]):
            ValueError("Il y a un probleme avec le nom des colonnes du Pandas DataFrame df.")

        df = self.df

        time0 = np.array(df.time0, dtype = pd.Series)
        time1 = np.array(df.time1, dtype = pd.Series)
        price0 = np.array(df.price0, dtype = pd.Series)
        price1 = np.array(df.price1, dtype = pd.Series)

        timevar = np.unique(np.concatenate((time0, time1), axis = 0))

        nt = len(timevar)
        n = len(price1)

        xmat = np.zeros((n, nt - 1))

        for j in range(1, nt):
            xmat[time1 == timevar[j], j - 1] = price1[time1 == timevar[j]]
            xmat[time0 == timevar[j], j - 1] = -price0[time0 == timevar[j]]

        if sum(np.isnan(xmat)).sum() != 0:
            print("Il y a des valeurs manquantes dans la design_matrix")

        return np.matrix(xmat)

def linear_model(xmat, dy):

    if not isinstance(xmat, np.matrix):
        xmat = np.matrix(xmat)
    if not isinstance(dy, np.matrix):
        dy = np.matrix(dy)

    beta = ((xmat.T * xmat) ** -1) * xmat.T * dy
    return beta

def linear_model_iv(xmat, zmat, y, asarray = False):

    if not isinstance(xmat, np.matrix):
        xmat = np.matrix(xmat)
    if not isinstance(zmat, np.matrix):
        zmat = np.matrix(zmat)
    if not isinstance(y, np.matrix):
        y = np.matrix(y)

    beta = ((zmat.T * xmat) ** -1) * zmat.T * y

    if asarray:
        return np.asarray(beta)
    else:
        return beta

def wrs_model(type, secteur, categorie, timevar, min_mnt = 50000,
                min_date = '20080101', max_date = '20180331', round = 4):
    """
    Cette fonction produit un HPI selon la methode du weighted repeat sales model.

    Args:
        type: Le type de zone geographique tel que 'rmr', 'ville', 'dr', etc.
        secteur: Le nom de la zone geographique relie au type tel que 'Montreal'
                 pour ville ou '1' pour 'ra'.
        categorie: La categorie de batiment tel que '2A', '3C', '2B' ou '2C'.
                   Doit etre une string pour une categorie unique ou une liste
                   de strings pour plusieurs categories.
        min_mnt: Le montant minimal a considere pour garder une vente. Par
                 defaut le montant minimal d'une transaction est 50 000 $.
        min_date: La date de debut de la periode chargee. Doit avoir un
                  format de date 'YYYYmmdd'.
        max_date: La date de fin de la periode chargee. Doit avoir un
                  format de date 'YYYYmmdd'.
        round: Le nombre de decimals apres la virgule pour la valeur de l'indice.

    Returns:
        Un Numpy ndarray contenant la valeur des coefficients representant les
        valeurs de l'indice.
    """
    d = Data_wrangling()
    d.load_dataset(type, secteur, categorie, min_mnt = 50000, min_date = min_date,
                    max_date = max_date)
    d.clean_SQL_output()
    d.rep_sale_data(timevar)

    # Verifier s'il y a des ventes pairees
    df = d.return_df()
    if len(df) == 0:
        print("Il y a pas de ventes pairees pour cette combinaison de parametres")
        return np.nan

    # First stage
    try:
        dy = np.matrix(df.price1 - df.price0).T
        xmat = d.design_matrix()
        beta = linear_model(xmat, dy)

    except (np.linalg.linalg.LinAlgError, MemoryError, ValueError) as err:
        print("Une erreur force l'algorithme a retourne une valeur NaN.")
        return np.nan

    else:
        # Second stage
        u = dy - xmat * beta
        x = (pd.to_datetime(df.time1) - pd.to_datetime(df.time0)).dt.days
        x = sm.add_constant(x)
        model = sm.OLS(np.absolute(u), x).fit()
        print(np.absolute(u).shape)
        wgt = model.predict()

        try:# Third stage
            fit = sm.WLS(dy, xmat, weights = wgt).fit()
            coef = np.append([0], fit.params)

            return np.round(np.exp(coef), decimals = round)

        except MemoryError:
            print("Il y a eu une MemoryError durant la troisieme etage du modele.")
            return np.nan

def iv_model(type, secteur, categorie, timevar, min_mnt = 50000,
                min_date = '20080101', max_date = '20180331', round = 4):
    """
    Cette fonction produit un HPI selon la methode par variable instrumentale.

    Args:
        type: Le type de zone geographique tel que 'rmr', 'ville', 'dr', etc.
        secteur: Le nom de la zone geographique relie au type tel que 'Montreal'
                 pour ville ou '1' pour 'ra'.
        categorie: La categorie de batiment tel que '2A', '3C', '2B' ou '2C'.
                   Doit etre une string pour une categorie unique ou une liste
                   de strings pour plusieurs categories.
        min_mnt: Le montant minimal a considere pour garder une vente. Par
                 defaut le montant minimal d'une transaction est 50 000 $.
        min_date: La date de debut de la periode chargee. Doit avoir un
                  format de date 'YYYYmmdd'.
        max_date: La date de fin de la periode chargee. Doit avoir un
                  format de date 'YYYYmmdd'.
        round: Le nombre de decimals apres la virgule pour la valeur de l'indice.

    Returns:
        Un Numpy ndarray contenant la valeur des coefficients representant les
        valeurs de l'indice.
    """
    d = Data_wrangling()
    d.load_dataset(type, secteur, categorie, min_mnt = 50000, min_date = min_date,
                    max_date = max_date)
    d.clean_SQL_output()
    d.rep_sale_data(timevar)

    # Verifier s'il y a des ventes pairees
    df = d.return_df()
    if len(df) == 0:
        print("Il y a pas de ventes pairees pour cette combinaison de parametres")
        return np.nan

    try:
        # Setup matrix
        zmat = d.design_matrix()
        xmat = d.design_matrix_iv()
        y = np.zeros([df.shape[0], ])
        y[df.time0 == np.sort(np.unique(df.time0))[0]] = df.price0[df.time0 == np.sort(np.unique(df.time0))[0]]
        y = np.matrix(y).T

        # First stage
        beta_first = linear_model_iv(xmat, zmat, y)

    except (np.linalg.linalg.LinAlgError, MemoryError, ValueError) as err:
        print("Une erreur force l'algorithme a retourne une valeur NaN.")
        return np.nan

    else:
        # Second Stage
        u = y - xmat * beta_first
        delta_time = (pd.to_datetime(df.time1) - pd.to_datetime(df.time0)).dt.days
        delta_time = sm.add_constant(delta_time)
        beta_second = linear_model(delta_time, np.absolute(u))
        wgt = np.asarray(1/(np.matrix(delta_time) * beta_second))

        zmat = np.asarray(zmat)
        z_omega = [zmat[i, :] * wgt[i] for i in np.arange(len(wgt))]

        try:
            # Third Stage
            beta_third = linear_model_iv(xmat, z_omega, y, asarray = True)
            coef = np.append([1], beta_third ** -1)
            import pdb; pdb.set_trace()
            return np.round(coef, decimals = round)
        except MemoryError:
            print("Une erreur force l'algorithme a retourne une valeur NaN.")
            return np.nan

# Test -------------------------------------------------------------------------
# def clean_data():
#     d = Data_wrangling()
#     d.load_dataset(type = 'rmr', secteur = 'montreal', categorie = ["'2A'", "'3C'", "'2B'", "'2C'"])
#     d.cleanSQLoutput()
#     d.repsaledata(timevar = 'month', priceLog = True)
#     d.design_matrix()
#     d.design_matrix_iv()
#     return d.return_df()
beta_iv = iv_model(type = 'rmr', secteur = 'montreal', categorie = ["'2A'"], timevar = "month", min_date = "20080101", max_date = "20180731")
import pdb; pdb.set_trace()
