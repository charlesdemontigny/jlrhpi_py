import matplotlib.pyplot as plt
import numpy as np

def tsplot(df, path):
    date = df.date
    indice = df.indice

    plt.plot(date, indice)
    plt.savefig('../data/processed/' + path + '.png')
