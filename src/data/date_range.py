
import pandas as pd
import numpy as np
import calendar
from datetime import datetime

# helper function to generate quarter sequence
def date_range_quarter(min_quarter, max_quarter):

    output = [min_quarter]

    current_quarter = min_quarter
    while current_quarter < max_quarter:

        year = int(current_quarter[0:4])
        quarter = int(current_quarter[5:6])

        if quarter < 4:
            current_quarter = str(year) + "-" + str(quarter + 1)

        if quarter == 4:
            current_quarter = str(year + 1) + "-1"

        output.append(current_quarter)

    return output

# helper function to generate semester sequence
def date_range_semester(min_semester, max_semester):

    output = [min_semester]

    current_semester = min_semester
    while current_semester < max_semester:

        year = int(current_semester[0:4])
        semester = int(current_semester[5:6])

        if semester == 1:
            current_semester = str(year) + "-2"
        if semester == 2:
            current_semester = str(year + 1) + "-1"

        output.append(current_semester)

    return output

# Create date range
def create_date_range(period, mindate, maxdate):
    """
    Cette fonction cree une sequence de date de mindate a maxdate
    en fonction du time period.
    """

    # Test condition
    from datetime import datetime

    condition_period = period in ("month", "quarter", "semester", "year")
    condition_mindate = mindate > "19900101"
    condition_maxdate = maxdate < datetime.now().strftime("%Y%m%d")

    assert condition_period, "La periode n'est pas valide."
    assert condition_mindate, "La date ne doit pas etre avant 1990-01-01."
    assert condition_maxdate, "La date ne doit pas etre apres celle d'aujourd'hui."

    # Date variable
    if period == 'month':
        date = pd.date_range(mindate, maxdate, freq = 'MS')
        date = pd.Series(date)
        date = date.dt.date.astype(str)

    if period == 'quarter':
        min_quarter = str(pd.to_datetime(mindate).year) + "-" + str(int(pd.Series(pd.to_datetime(mindate)).dt.quarter))
        max_quarter = str(pd.to_datetime(maxdate).year) + "-" + str(int(pd.Series(pd.to_datetime(maxdate)).dt.quarter))
        date = date_range_quarter(min_quarter, max_quarter)

    if period == "semester":
        min_semester = str(pd.to_datetime(mindate).year) + "-"
        if int(pd.Series(pd.to_datetime(mindate)).dt.quarter) <= 2:
            min_semester += "1"
        else: min_semester += "2"

        max_semester = str(pd.to_datetime(maxdate).year) + "-"
        if int(pd.Series(pd.to_datetime(maxdate)).dt.quarter) <= 2:
            max_semester += "1"
        else: max_semester += "2"
        date = date_range_semester(min_semester, max_semester)

    if period == 'year':
        min_year = pd.to_datetime(mindate).year
        max_year = pd.to_datetime(maxdate).year + 1
        date = np.arange(min_year, max_year).astype(str)

    return date

# Date to Period
def date_to_period(date, period):
    """
    Cette fonction transforme une date format YYYYmmdd en
    periode de temps: month, quarter, semester, year.
    """

    # Testing
    condition_period = period in ("month", "quarter", "semester", "year")
    assert condition_period, "La periode n'est pas valide."

    date = date.replace("-", "")

    # Month
    if period == 'month':
        sub_date = date[0:6]
        return sub_date

    # Quarter
    if period == 'quarter':
        year = date[0:4]
        month = int(date[5:7])

        if month <= 3:
            quarter = "1"
        elif month <= 6:
            quarter = "2"
        elif month <= 9:
            quarter = "3"
        elif month >= 10 and month <= 12:
            quarter = "4"

        return "-".join([year, quarter])

    # Semester
    if period == 'semester':
        year = date[0:4]
        month = int(date[5:7])

        if month <= 6:
            semester = "1"
        elif month >= 7 and month <= 12:
            semester = "2"

        return "-".join([year, semester])

    # Year
    if period == 'year':
        sub_date = date[0:4]
        return sub_date

# last_month
def last_month(as_character = False):
    """
    Cette fonction retourne la valeur du dernier mois. La deuxieme partie du code
    prend en compte le mois de janvier.
    """

    if datetime.now().month != 1:
        month = datetime.now().month - 1
        if not as_character:
            return month
        if as_character:
            if len(str(month)) == 1:
                month = str(0) + str(month)
                return month
            else: return str(month)

    if datetime.now().month == 1:
        month = 12
        if not as_character:
            return month
        if as_character:
            return str(month)

# Last day of last month
def last_day_last_month():
    """
    Return the last day of the last month.
    """
    this_year = int(datetime.now().strftime("%Y"))
    last_month_num = last_month()
    last_day = str(calendar.monthrange(this_year, last_month_num)[1])

    return "".join([str(this_year), last_month(True), last_day])

# This is 1st day
def this_year_1st_day():
    """
    Return the first day of this year.
    """
    this_year = str(datetime.now().strftime("%Y"))

    return "".join([this_year, "0101"])

def this_year_last_day():
    """
    Return the last day of this year.
    """
    this_year = str(datetime.now().strftime("%Y"))

    return "".join([this_year, "1231"])
