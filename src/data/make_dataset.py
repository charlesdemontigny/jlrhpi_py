
# Import packages --------------------------------------------------------------
import cx_Oracle
import click
import pandas as pd

from data.date_range import date_range_quarter, date_range_semester

# load_dataset function --------------------------------------------------------
def load_dataset(typ, secteur,categorie, min_mnt = 50000, min_date = '20080101',
                max_date = '20180331', print = True):

    # Testing
    condition_type = (typ == 'TOUT' or typ == 'rmr' or typ == 'ville' or typ == 'dr' or typ == 'ra' or typ == 'arr')
    assert condition_type, "Le type n'est pas conforme."

    # Variables settings
    rmr = {'quebec':421, 'saguenay':408, 'sherbrooke':433, 'trois-rivieres': 442, 'montreal':462, 'gatineau':505}

    # Oracle connection and query
    if print:
        click.secho(f"\nInitialisation de la connexion et creation de la requete.", bold=True)
    connection = cx_Oracle.connect( "jlrprod", 'rlj','azuorap3', encoding = 'UTF-8')

    query = """select TRA_TRANSACTION.TRA_MNT_TRANSACTION, TRA_TRANSACTION.TRA_DT_PUBLIE, TRA_TRANSACTION.TRA_ID_UEF, TRA_TRANSACTION.TRA_CAT_BAT
                from tra_transaction
                join municipalites_bsq on TRA_TRANSACTION.IMM_BSQ = MUNICIPALITES_BSQ.MUN_NO_BSQ
                join municipalites_statcan_bsq on TRA_TRANSACTION.IMM_BSQ = MUNICIPALITES_STATCAN_BSQ.MUN_NO_BSQ
                where tra_type=4 and tra_cat_bat in (""" + categorie + """) and to_char(tra_dt_publie,
                        'YYYYmmdd')> '""" + min_date + """' and
                        to_char(tra_dt_publie, 'YYYYmmdd') <= '""" + max_date +"""' and
                        TRA_IND_VENTE_MULTIPLE=0
                        and TRA_IND_VENTE_REPRISE=0
                        and TRA_IND_VENTE_LIE=0
                        and TRA_MNT_TRANSACTION >"""  + str(min_mnt)

    # Cette partie du code ajoute la possibilite de prendre plusieurs catbat dans
    # la requete.

    # Cette partie permet de filtrer sur des zones geographiques differentes.
    if typ == 'rmr':
        query += """ and municipalites_bsq.MUN_NO_RMR = """ + str(rmr[secteur])

    if typ == 'ville':
        query += """ and municipalites_bsq.MUN_BSQ_FUSION = """ + str(secteur)

    if typ == 'dr':
        query += """ and DRIDU = """ + str(secteur)

    if typ == 'ra':
        query += """ and municipalites_bsq.MUN_NO_RA = """ + str(secteur)

    if typ == 'arr':
        query += """and imm_arv_id =  """ + str(secteur)
        query += """and imm_bsq_nom in ('MONTREAL','LEVIS')"""

    #return query
    if print:
        click.secho(f"\nDebut du telechargement des donnees a partir de Oracle.", bold=True)
    #print(query)
    return pd.read_sql(query.encode('utf-8'), con = connection)
