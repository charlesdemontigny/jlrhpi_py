"""
@date: 2018-04-23
@title: HPI Indexation
@author: Charles Demontigny
@version: Python 3.6
"""

"""
Dans ce script, on retrouvera une class avec des méthodes pour faire l'Indexation
d'un prix de vente grace au hpi.
"""
# Import packages --------------------------------------------------------------
import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_log_error, mean_absolute_error, mean_squared_error

# Indexation -------------------------------------------------------------------
class indexation(object):

    def __init__(self, df, geo_var_df, hpi, date_t0 = "time0", date_t1 = "time1", geo_var_hpi = "desc_geo", date_hpi = "date"):
        """Assigner les deux DataFrames a self puis cree le DataFrame pour l'indexation."""
        self.df = keys_creation_df(df = df, geo_var = geo_var_df, date_t0 = date_t0, date_t1 = date_t1)
        self.hpi = key_hpi(hpi, geo_var_hpi, date_hpi)
        self.df_final = merge_hpi_t_t1(self.df, self.hpi)

    def predict(self, price0):
        """
        Procede a un calcul complexe d'indexation pour faire une prevision
        du prix de vente.
        """

        df = self.df_final
        # Si prev_price n'est pas NaN faire le calcul
        condition = ~(df[price0].isnull())

        # Get location of variables for indexation
        loc_price_t0 = df.columns.get_loc(price0)
        loc_indice_t0 = df.columns.get_loc('indice_t0')
        loc_indice_t1 = df.columns.get_loc('indice_t1')

        # Initialisation du vecteur a remplir
        index_price = []

        # Loop over every row of the df
        for row, cond in zip(df.itertuples(), condition):
            if cond:
                index_price.append((row[loc_price_t0 + 1] /
                                    row[loc_indice_t0 + 1]) * row[loc_indice_t1 + 1])
            else:
                index_price.append(np.nan)

        df['index_price'] = index_price
        self.output = df
        return df

    def eval(self, price1):
        """Compute metrics on the prediction."""

        df = self.output
        df = df[[price1, 'index_price']]
        subset = df.dropna(axis = 0, how = 'any')

        return compute_metrics(subset, price1, 'index_price')


# Data cleaning ----------------------------------------------------------------

# Helper function
def key_helper(df, geo_var, date, loc_geo, loc, output_var):
    condition = ~(df[geo_var].isnull() | df[date].isnull())
    key = []
    for row, cond in zip(df.itertuples(), condition):
        if cond:
            key.append(str(row[loc_geo + 1]) + '-' + str(row[loc + 1]))
        else:
            key.append(np.nan)
    df[output_var] = key
    return df

# keys creation for df
def keys_creation_df(df, geo_var, date_t0, date_t1):
    """Prend un df et ajoute des cles pour merge avec hpi"""

    # find location of variables
    loc_geo = df.columns.get_loc(geo_var)
    loc_t0 = df.columns.get_loc(date_t0)
    loc_t1 = df.columns.get_loc(date_t1)
    #loc_catbat = df.columns.get_loc(catbat)

    # key_t0
    df = key_helper(df = df, geo_var = geo_var, date = date_t0,
                    loc_geo = loc_geo, loc = loc_t0,
                     output_var = 'key_t0')

    # key_t1
    df = key_helper(df = df, geo_var = geo_var, date = date_t1,
                    loc_geo = loc_geo, loc = loc_t1,
                     output_var = 'key_t1')
    return df

# key for hpi
def key_hpi(hpi, geo_var, date):
    """ Creer une cle similaire a keys_creation_df pour le merge"""
    hpi.loc[:, 'key_hpi'] = hpi['desc_geo'].astype(str) + '-' + hpi['date'].astype(str)
    return hpi

# Merge df and hpi to have the final DataFrame
def merge_hpi_t_t1(df, hpi):
    """Merge df avec hpi sur les deux cles"""
    # Merge en t1 (get it: en t, hante...)
    temp = pd.merge(df, hpi[['indice', 'key_hpi']],
                    how = 'left', left_on = 'key_t1', right_on = 'key_hpi')
    temp.rename(columns = {'indice' : 'indice_t1'}, inplace = True)
    # Merge en t0 (pas de blague)
    output = pd.merge(temp, hpi[['indice', 'key_hpi']],
                    how = 'left', left_on = 'key_t0', right_on = 'key_hpi')
    output.rename(columns = {'indice' : 'indice_t0'}, inplace = True)
    return output

# Metrics ----------------------------------------------------------------------

def MAPE(y_true, y_pred):
    '''Mean absolute percentage error'''

    if not (y_true != 0).all():
        raise ValueError("Mean absolute percentage error cannot be used when "
                         "targets contain zero values.")

    return np.mean(np.abs((y_true - y_pred) / (y_true)))

def MedAPE(y_true, y_pred):
    '''Median absolute percentage error'''

    if not (y_true != 0).all():
        raise ValueError("Median absolute percentage error cannot be used when "
                         "targets contain zero values.")

    return np.median(np.abs((y_true - y_pred) / (y_true)))

def RMSE(y_true, y_pred):
    '''Root mean squared error'''
    return np.sqrt(mean_squared_error(y_true, y_pred))

def compute_metrics(df, y_true, y_pred):
    metrics = {
        'MAPE': MAPE,
        'MedAPE': MedAPE,
        "RMSE": RMSE}
    return pd.Series({
        name: metric(df[y_true], df[y_pred])
        for name, metric in metrics.items()
    })
