"""
@date: 2018-04-23
@title: Backtesting HPI prediction
@author: Charles Demontigny
@version: Python 3.6
"""

# Import modules ---------------------------------------------------------------
import pandas as pd
import numpy as np
import cx_Oracle
import click

from models.hpi_models import cleanSQLoutput, repsaledata
from predict.hpi_indexation import indexation

# Load data --------------------------------------------------------------------

click.secho(f"\nChargement des transactions et des HPIs...", bold = True)

# Setup connection
connection = cx_Oracle.connect( "jlrprod", 'rlj','azuorap3', encoding = 'UTF-8')

# Generate a query
query = """select TRA_ID_UEF, TRA_NUMERO, TRA_MNT_TRANSACTION, TRA_DT_PUBLIE, TRA_CAT_BAT, municipalites_bsq.MUN_NO_RMR,
            municipalites_bsq.MUN_NO_RA, DRIDU, municipalites_bsq.MUN_BSQ_FUSION, IMM_ARV_ID
           from TRA_TRANSACTION
                join municipalites_bsq on TRA_TRANSACTION.IMM_BSQ = MUNICIPALITES_BSQ.MUN_NO_BSQ
                join municipalites_statcan_bsq on TRA_TRANSACTION.IMM_BSQ = MUNICIPALITES_STATCAN_BSQ.MUN_NO_BSQ
           where TRA_TYPE = 4
           and TRA_CAT_BAT in ('2A', '3C', '2B', '2C')
           and to_char(TRA_DT_PUBLIE, 'YYYYmmdd') > '19900101'
           and TRA_IND_VENTE_MULTIPLE=0
           and TRA_IND_VENTE_REPRISE=0
           and TRA_IND_VENTE_LIE=0
           and TRA_MNT_TRANSACTION > 50000
        """
# Load data from Oracle
df = pd.read_sql(query.encode('utf-8'), con = connection)
df.to_csv("../data/interim/backtesting_df.csv")

# df = pd.read_csv("../data/interim/backtesting_df.csv")

# Load HPIs data
hpi = pd.read_csv("../data/processed/test_backtesting.csv")

# Clean data -------------------------------------------------------------------

click.secho(f"\nNettoyage des donnees...", bold = True)

# Passer a un DataFrame de ventes pairees
df['TRA_DT_PUBLIE'] = pd.to_datetime(df['TRA_DT_PUBLIE'])
df_clean = cleanSQLoutput(df, num = True)
rps = repsaledata(df_clean.id_, df_clean.price, df_clean.year, df_clean.numero, priceLog = False, num = True)

# Garder ceux que la vente t1 est apres 20150101
rps2015 = rps.query("time1 >= '2015'")

# Geographie
characteristics = df[['TRA_ID_UEF', 'TRA_NUMERO', 'TRA_CAT_BAT', "DRIDU", "MUN_NO_RA"]]
#characteristics.drop_duplicates('TRA_ID_UEF', inplace = True)
df_merged = pd.merge(rps2015, characteristics, how ='left', left_on = "numero", right_on = "TRA_NUMERO")

# Remove le DR de Perce
df_final = df_merged.query("DRIDU != 2402")

# Merge, predict and evaluate --------------------------------------------------
# Prendre la class indexation de hpi_indexation.py pour creer toutes les options
# possible de prediction.
prediction_ra = indexation(df_final, geo_var_df = 'MUN_NO_RA', hpi = hpi)
pred_ra = prediction_ra.predict("price0")
eval_ra = prediction_ra.eval("price1")
print(eval_ra)

prediction_dr = indexation(df_final, geo_var_df = 'DRIDU', hpi = hpi)
pred_dr = prediction_dr.predict("price0")
eval_dr = prediction_dr.eval("price1")
print(eval_dr)

# Export result ----------------------------------------------------------------
pred_ra.to_csv("../data/processed/pred_ra.csv", index = False)
pred_dr.to_csv("../data/processed/pred_dr.csv", index = False)
print("Prediction sauvegardées!")
