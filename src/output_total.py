"""
Created on Thu Oct 26 09:31:21 2017
@title: JLR HPI
@author: Charles Demontigny
@version: Python 3.6
"""

# Import packages ---------------------------------------------------------
import pandas as pd
import numpy as np
import os
import statsmodels.api as sm
import sys
import click
import time
import itertools
from datetime import datetime

import models.hpi_models as hp
from models.jlrhpi import jlrhpi, multi_jlrhpi
from data.make_dataset import load_dataset, date_range_quarter
from models.hpi_models import rolling_mean

# Setup input -------------------------------------------------------------

t0 = time.time()

#print("Initialisation des variables pour le HPI.")

# Secteur
rmr = ['montreal', 'quebec', 'trois-rivieres', 'gatineau', 'sherbrooke', 'saguenay']

ra = list(range(1, 18))

dr = pd.read_csv("../data/raw/driud.txt")

ville = pd.read_csv("../data/raw/ville_2A.txt", encoding='latin1')

arr = pd.read_csv("../data/raw/arrondissements.csv", encoding='latin1', sep = ";")

# Categorie
catbat = ["'2A'", "'3C'", "'2A', '3C'", "'2B', '2C'", "'2A', '2B', '2C', '3C'"]

# Period
period = ['year', 'semester', 'quarter', 'month']

# TEST --------------------------------------------------------------------

# df = multi_jlrhpi(df = None, type_ = "dr", secteur =[2490],
#                 categorie = ["'3C'"], period = ['year'],
#                 mindate = '20040101')
catbat = ["'2A'", "'2A', '3C'", "'3C'"]

df = multi_jlrhpi(df = None, model = "iv", type_ = "rmr", secteur = ['quebec'],
                categorie = ["'2A'"], period = ['year'],
                mindate = '20080101', maxdate = '20111231', verbose = True)
# df = multi_jlrhpi(df = df, type_ = "dr", secteur = dr.dr,
#                 categorie = catbat, period = ['year'],
#                 mindate = '19900101', maxdate = '20180430', verbose = True)
# df = multi_jlrhpi(df = df, type_ = 'ville', secteur = ville.code,
#                 categorie = catbat, period = ['year'],
#                 mindate = '19900101', maxdate = '20180430', verbose = True)
# df = multi_jlrhpi(df = df, type_ = "arr", secteur = arr.arr,
#                 categorie = catbat, period = ['year'],
#                 mindate = '19900101', maxdate = '20180430', verbose = True)

# Multi jlrhpi ------------------------------------------------------------

# # Province
# df = multi_jlrhpi(df = None, type_ = 'TOUT', secteur = ['quebec'],
#                     categorie = catbat, period =  period, rolling_mean = True,
#                     mindate = '20040101', maxdate = '20180430')
#
# # RMR
# df = multi_jlrhpi(df = df, type_ = 'rmr', secteur = rmr,
#                     categorie = catbat, period = period, rolling_mean = True,
#                     mindate = '20040101', maxdate = '20180430')
#
# # Region Administrative
# df = multi_jlrhpi(df = df, type_ = "ra", secteur = ra,
#                     categorie = catbat, period = period, rolling_mean = True,
#                     mindate = '20040101', maxdate = '20180430')
#
# # Division de recensement
# df = multi_jlrhpi(df = df, type_ = "dr", secteur = dr.dr,
#                     categorie = catbat, period = period, rolling_mean = True,
#                     mindate = '20040101', maxdate = '20180430')
#
# # Villes
# df = multi_jlrhpi(df = df, type_ = 'ville', secteur = ville.code,
#                     categorie = catbat, period = period, rolling_mean = True,
#                     mindate = '20040101', maxdate = '20180430')
#
# # Arrondissements
# df = multi_jlrhpi(df = df, type_ = 'arr', secteur = arr.arr,
#                     categorie = catbat, period = period, rolling_mean = True,
#                     mindate = '20040101', maxdate = '20180430')

# Save output -------------------------------------------------------------

print(df)

#path = "hpi_" + datetime.now().date().strftime("%Y-%m-%d") + '.csv'
# path = 'test_date.csv'
# df.to_csv("../data/processed/" + path, index = False)
#
# # Process time -------------------------------------------------------------
# t1 = time.time()
# delta = np.round((t1 - t0)/60, decimals = 2)
# print("Terminé! Le HPI a pris {} minutes a rouler.".format(delta))
